<?php

Route::domain('{account}.pichaclick.co.ke')->group(function () {
    Route::get('/', function ($account) {
        $user = \App\User::all()->where('subdomain', $account)->first();
        if (!$user->subdomain) {
            return abort(404, 'This photographers Gallery does not exist.');
        } else {
            $collections = \App\Collection::withoutGlobalScope('user')->where('user_id', $user->id)->get();

            return view('gallery.index', compact('user', 'collections'));

        }
    });
    Route::get('user/{id}', function ($account, $id) {
        return \App\User::all();
    });
});
Route::domain('{account}.localhost')->group(function () {
    Route::get('/', function ($account) {
        $user = \App\User::all()->where('subdomain', $account)->first();
        if (!$user->subdomain) {
            return abort(404, 'This photographers Gallery does not exist.');
        } else {
            $collections = \App\Collection::withoutGlobalScope('user')->where('user_id', $user->id)->get();

            return view('gallery.index', compact('user', 'collections'));

        }
    });
    Route::get('user/{id}', function ($account, $id) {
        return \App\User::all();
    });
});
Route::get('pic', 'HomeController@pic');
Route::get('/', function () {
    return view('welcome');
});

Route::get('gallery/{gallery}', 'HomeController@gallery')->name('gallery.show');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {

    Route::resource('collection', 'CollectionController');
    Route::resource('uploadPhoto', 'CollectionphotoController');
    Route::view('collection/1', 'collection.show');
    Route::view('settings', 'settings.index');

});

Auth::routes();

Route::view('sendcollection', 'collection.send');

Route::get('install', function () {
    Artisan::call('migrate:fresh');
    return Artisan::output();
});

Route::post('earlyaccess', function (\Illuminate\Http\Request $request) {
    $mail = Mail::to([$request->email, 'kenmsh@gmail.com'])->send(new \App\Mail\EarlyAccess());
    if (Mail::failures()) {
        return response()->json('Failed');
    }
    return response()->json("Awesome");
});


Route::post('mail', function (\Illuminate\Http\Request $request) {
    $mail = Mail::to('kenmsh@gmail.com')->send(new \App\Mail\ContactUs($request->contactemail, $request->contactphone,
        $request->contactmessage, $request->contactname));

    return $mail;
});

Route::post("sendinvite", function (\Illuminate\Http\Request $request) {
    $subject = $request->subjectLine;
    $message = $request->message;
    $emails = $request->email;
    $mail = Mail::to([$emails])->send(new \App\Mail\PhotosReady($subject, $message));
    if (Mail::failures()) {
        return response()->json("Failures");
    }
    return response()->json("Success");

});

Route::get('sitemap', function () {
    return response(file_get_contents(base_path('sitemap.xml')))->header('Content-Type', 'text/xml');
});

Route::get('.well-known/.well-known/acme-challenge/YpMg0GXN4eQiNhYfSbnShE1iijHIy3fnwA_fIO7owJs', function () {
    return "YpMg0GXN4eQiNhYfSbnShE1iijHIy3fnwA_fIO7owJs.BmHqbF0nx7gHtAEh01ItfOoL0D2213-Tfzwh9xsPXio";
});


Route::get('users', function () {
    $users = \App\User::all();

    return \App\User::all();
});
//intelliagent
//openoffice
//vivadigital
//saMSTON31@GMAIL.COM
//GTIPAY UGANDA
//UNITECHMS.COM
