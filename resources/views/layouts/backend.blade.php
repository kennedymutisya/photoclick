<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="apppath" content="{{ public_path() }}">


    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="theme-color" content="#017df7"/>
    <meta name="msapplication-navbutton-color" content="#017df7">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#017df7">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://unpkg.com/vue2-animate/dist/vue2-animate.min.css"/>
    <!-- Custom CSS -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-54081021-8"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-54081021-8');
    </script>
</head>
<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="loader-pendulums"></div>
</div>
<!-- /Preloader -->

<!-- HK Wrapper -->
<div class="hk-wrapper hk-horizontal-nav">
    <!-- Top Navbar -->
    <nav class="navbar navbar-expand-xl navbar-dark fixed-top hk-navbar">
        <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span
                class="feather-icon"><i data-feather="menu"></i></span></a>
        <a class="navbar-brand" href="#">


            <img class="brand-img d-inline-block" src="{{ asset('logo1.png') }}" alt="brand"/>
        </a>
        <ul class="navbar-nav hk-navbar-content">
            <li class="nav-item">
                <a id="navbar_search_btn" class="nav-link nav-link-hover" href="javascript:void(0);"><span
                        class="feather-icon"><i data-feather="search"></i></span></a>
            </li>
            <li class="nav-item">
                <a id="settings_toggle_btn" class="nav-link nav-link-hover" href="javascript:void(0);"><span
                        class="feather-icon"><i data-feather="settings"></i></span></a>
            </li>
            <li class="nav-item dropdown dropdown-notifications">
                <a class="nav-link dropdown-toggle no-caret" href="#" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false"><span class="feather-icon"><i
                            data-feather="bell"></i></span><span class="badge-wrap"><span
                            class="badge badge-primary badge-indicator badge-indicator-sm badge-pill pulse"></span></span></a>
                <div class="dropdown-menu dropdown-menu-right" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                    <h6 class="dropdown-header">Notifications <a href="javascript:void(0);" class="">View all</a></h6>
                    <div class="notifications-nicescroll-bar">
                    </div>
                </div>
            </li>
            <li class="nav-item dropdown dropdown-authentication">
                <a class="nav-link dropdown-toggle no-caret" href="#" role="button" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <div class="media">
                        <div class="media-img-wrap">
                            <div class="avatar">
                                <img src="{{ asset('avatar.png') }}" alt="user"
                                     class="avatar-img rounded-circle">
                            </div>
                            <span class="badge badge-success badge-indicator"></span>
                        </div>
                        <div class="media-body">
                            <span>{{ Auth::user()->name }}<i class="zmdi zmdi-chevron-down"></i></span>
                        </div>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-right" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">

                    <div class="dropdown-divider"></div>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        <i class="dropdown-icon zmdi zmdi-power"></i>
                        <span>Log out</span>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </a>
                </div>
            </li>
        </ul>
    </nav>
    <form role="search" class="navbar-search">
        <div class="position-relative">
            <a href="javascript:void(0);" class="navbar-search-icon"><span class="feather-icon"><i
                        data-feather="search"></i></span></a>
            <input type="text" name="example-input1-group2" class="form-control" placeholder="Type here to Search">
            <a id="navbar_search_close" class="navbar-search-close" href="#"><span class="feather-icon"><i
                        data-feather="x"></i></span></a>
        </div>
    </form>
    <!-- /Top Navbar -->

    <!--Horizontal Nav-->
    <nav class="hk-nav hk-nav-light">
        <a href="javascript:void(0);" id="hk_nav_close" class="hk-nav-close"><span class="feather-icon"><i
                    data-feather="x"></i></span></a>
        <div class="nicescroll-bar">
            <div class="navbar-nav-wrap">
                <ul class="navbar-nav flex-row">
                    <li class="nav-item">
                        <a class="nav-link link-with-badge" href="{{ route('collection.index') }}">
                            <span class="feather-icon">
                                <i data-feather="eye"></i>
                            </span>
                            <span class="nav-link-text">Collections</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link link-with-badge" href="{{ url('settings') }}">
                            <span class="feather-icon"><i data-feather="settings"></i></span>
                            <span class="nav-link-text">Settings</span>
                        </a>
                    </li>
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link link-with-badge" href="#">--}}
{{--                            <span class="feather-icon"><i data-feather="sliders"></i></span>--}}
{{--                            <span class="nav-link-text">Tools</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="nav-item">--}}
{{--                        <a class="nav-link link-with-badge" href="#">--}}
{{--                            <span class="feather-icon"><i data-feather="shopping-cart"></i></span>--}}
{{--                            <span class="nav-link-text">Store</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    <li class="nav-item pull-right">
                            <a class="nav-link link-with-badge" target="_blank" href="https://{{Auth::user()
                            ->subdomain ?? ''}}.pichaclick.co.ke">
                                <span class="feather-icon"><i data-feather="link"></i></span>
                                <span class="nav-link-text">Show me my Gallery</span>
                            </a>


                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <div id="hk_nav_backdrop" class="hk-nav-backdrop"></div>
    <!--/Horizontal Nav-->

    <!-- Setting Panel -->
    <div class="hk-settings-panel">
        <div class="nicescroll-bar position-relative">
            <div class="settings-panel-wrap">
                <div class="settings-panel-head">
                    <img class="brand-img d-inline-block align-top" src="{{ asset('logoblack.png') }}"
                         alt="brand"/>
                    <a href="javascript:void(0);" id="settings_panel_close" class="settings-panel-close"><span
                            class="feather-icon"><i data-feather="x"></i></span></a>
                </div>
                <hr>
                <h6 class="mb-5">Layout</h6>
                <p class="font-14">Choose your preferred layout</p>
                <div class="layout-img-wrap">
                    <div class="row">
                        <a href="#" class="col-6 mb-30">
                            <img class="rounded opacity-70" src="{{ asset('dist/img/layout1.png') }}" alt="layout">
                            <i class="zmdi zmdi-check"></i>
                        </a>
                        <a href="javascript:void(0);" class="col-6 mb-30 active">
                            <img class="rounded opacity-70" src="{{ asset('dist/img/layout2.png') }}" alt="layout">
                            <i class="zmdi zmdi-check"></i>
                        </a>
                        <a href="#" class="col-6 mb-30">
                            <img class="rounded opacity-70" src="{{ asset('dist/img/layout3.png') }}" alt="layout">
                            <i class="zmdi zmdi-check"></i>
                        </a>
                        <a href="#" class="col-6 mb-30">
                            <img class="rounded opacity-70" src="{{ asset('dist/img/layout4.png') }}" alt="layout">
                            <i class="zmdi zmdi-check"></i>
                        </a>
                        <a href="#" class="col-6">
                            <img class="rounded opacity-70" src="{{ asset('dist/img/layout5.png') }}" alt="layout">
                            <i class="zmdi zmdi-check"></i>
                        </a>
                    </div>
                </div>
                <hr>
                <h6 class="mb-5">Navigation</h6>
                <p class="font-14">Menu comes in two modes: dark & light</p>
                <div class="button-list hk-nav-select mb-10">
                    <button type="button" id="nav_light_select"
                            class="btn btn-outline-primary btn-sm btn-wth-icon icon-wthot-bg"><span
                            class="icon-label"><i class="fa fa-sun-o"></i> </span><span
                            class="btn-text">Light Mode</span></button>
                    <button type="button" id="nav_dark_select"
                            class="btn btn-outline-light btn-sm btn-wth-icon icon-wthot-bg"><span class="icon-label"><i
                                class="fa fa-moon-o"></i> </span><span class="btn-text">Dark Mode</span></button>
                </div>
                <hr>
                <h6 class="mb-5">Top Nav</h6>
                <p class="font-14">Choose your liked color mode</p>
                <div class="button-list hk-navbar-select mb-10">
                    <button type="button" id="navtop_light_select"
                            class="btn btn-outline-light btn-sm btn-wth-icon icon-wthot-bg"><span class="icon-label"><i
                                class="fa fa-sun-o"></i> </span><span class="btn-text">Light Mode</span></button>
                    <button type="button" id="navtop_dark_select"
                            class="btn btn-outline-primary btn-sm btn-wth-icon icon-wthot-bg"><span
                            class="icon-label"><i class="fa fa-moon-o"></i> </span><span
                            class="btn-text">Dark Mode</span></button>
                </div>
                <hr>
                <div class="d-flex justify-content-between align-items-center">
                    <h6>Scrollable Header</h6>
                    <div class="toggle toggle-sm toggle-simple toggle-light toggle-bg-primary scroll-nav-switch"></div>
                </div>
                <button id="reset_settings" class="btn btn-primary btn-block btn-reset mt-30">Reset</button>
            </div>
        </div>
        <img class="d-none" src="{{ asset('dist/img/logo-light.png') }}" alt="brand"/>
        <img class="d-none" src="{{ asset('dist/img/logo-dark.png') }}" alt="brand"/>
    </div>
    <!-- /Setting Panel -->

    <!-- Main Content -->
    <div class="hk-pg-wrapper" id="app">
        @yield('content')
    </div>
    <!-- /Container -->

    <!-- Footer -->
    <div class="hk-footer-wrap container">
        <footer class="footer">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <p>PichaClick © {{ date('Y') }}</p>
                    <br>
                </div>
                <div class="col-md-6 col-sm-12">
                    <p class="d-inline-block">Follow us</p>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span
                            class="btn-icon-wrap"><i class="fa fa-facebook"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span
                            class="btn-icon-wrap"><i class="fa fa-twitter"></i></span></a>
                    <a href="#" class="d-inline-block btn btn-icon btn-icon-only btn-indigo btn-icon-style-4"><span
                            class="btn-icon-wrap"><i class="fa fa-google-plus"></i></span></a>
                </div>
            </div>
        </footer>
    </div>
    <!-- /Footer -->
</div>
<!-- /Main Content -->

<!-- /HK Wrapper -->
<!-- jQuery -->
{{--<script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>--}}

<!-- Bootstrap Core JavaScript -->
{{--<script src="{{ asset('vendors/popper.js/dist/umd/popper.min.js') }}"></script>--}}
{{--<script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>--}}
<script src="{{ asset('js/app.js') }}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{ asset('dist/js/jquery.slimscroll.js') }}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{ asset('dist/js/dropdown-bootstrap-extended.js') }}"></script>

<!-- FeatherIcons JavaScript -->
<script src="{{ asset('dist/js/feather.min.js') }}"></script>

<!-- Toggles JavaScript -->
<script src="{{ asset('vendors/jquery-toggles/toggles.min.js') }}"></script>
<script src="{{ asset('dist/js/toggle-data.js') }}"></script>

<!-- Toastr JS -->
<script src="{{ asset('vendors/jquery-toast-plugin/dist/jquery.toast.min.js') }}"></script>

<!-- Counter Animation JavaScript -->
<script src="{{ asset('vendors/waypoints/lib/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('vendors/jquery.counterup/jquery.counterup.min.js') }}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{ asset('vendors/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('vendors/morris.js/morris.min.js') }}"></script>

<!-- Easy pie chart JS -->
<script src="{{ asset('vendors/easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>

<!-- Flot Charts JavaScript -->
<script src="{{ asset('vendors/flot/excanvas.min.js') }}"></script>
<script src="{{ asset('vendors/flot/jquery.flot.js') }}"></script>
<script src="{{ asset('vendors/flot/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('vendors/flot/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('vendors/flot/jquery.flot.time.js') }}"></script>
<script src="{{ asset('vendors/flot/jquery.flot.stack.js') }}"></script>
<script src="{{ asset('vendors/flot/jquery.flot.crosshair.js') }}"></script>
<script src="{{ asset('vendors/jquery.flot.tooltip/js/jquery.flot.tooltip.min.js') }}"></script>

<!-- EChartJS JavaScript -->
<script src="{{ asset('vendors/echarts/dist/echarts-en.min.js') }}"></script>

<!-- Init JavaScript -->
<script src="{{ asset('dist/js/init.js') }}"></script>
<script src="{{ asset('dist/js/dashboard2-data.js') }}"></script>

</body>
<!-- Mirrored from hencework.com/theme/mintos/dashboard2.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 14:09:52 GMT -->
</html>
