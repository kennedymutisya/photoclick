<!DOCTYPE html>
<!--
Template Name: Mintos - Responsive Bootstrap 4 Admin Dashboard Template
Author: Hencework
Contact: https://hencework.ticksy.com/

License: You must have a valid license purchased only from templatemonster to legally use the template for your project.
-->
<html lang="en">


<!-- Mirrored from hencework.com/theme/mintos/signup-simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 14:11:16 GMT -->
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>PichaClick Sign up</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework"/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Custom CSS -->
    <link href="{{ asset('dist/css/style.css') }}" rel="stylesheet" type="text/css">
</head>

<body>
<!-- Preloader -->
<div class="preloader-it">
    <div class="loader-pendulums"></div>
</div>
<!-- /Preloader -->

<!-- HK Wrapper -->
<div class="hk-wrapper">

    <!-- Main Content -->
    <div class="hk-pg-wrapper hk-auth-wrapper">
        <header class="d-flex justify-content-end align-items-center">
            <div class="btn-group btn-group-sm">
{{--                <a href="#" class="btn btn-outline-secondary">Help</a>--}}
                <a href="/login" class="btn btn-outline-secondary">Login</a>
            </div>
        </header>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 pa-0">
                    <div class="auth-form-wrap pt-xl-0 pt-70">
                        <div class="auth-form w-xl-30 w-lg-55 w-sm-75 w-100">
                            <a class="auth-brand text-center d-block mb-20" href="/">
                                <img class="brand-img" src="{{ asset('logoblack.png') }}" alt="brand"/>
                            </a>
                            <p style="color: #666; line-height: 1.5em;" class="content-end"><b>Sign up for free.</b> No credit card
                                required.</p>
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="form-group ">
                                    {{--                                    <label for="name" class=" col-form-label text-md-right">{{ __('Name') }}</label>--}}

                                    <input id="businessname" type="text"
                                           class="form-control{{ $errors->has('businessname') ? ' is-invalid' : '' }}"
                                           placeholder="Business Name"
                                           name="businessname" value="{{ old('businessname') }}" required autofocus>

                                    @if ($errors->has('businessname'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('businessname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group ">
                                    {{--                                    <label for="name" class=" col-form-label text-md-right">{{ __('Name') }}</label>--}}

                                    <input id="name" type="text"
                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           placeholder="Your Name"
                                           name="name" value="{{ old('name') }}">

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group ">
                                    {{--<label for="email"--}}
                                    {{--class=" col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                                    <div class="">
                                        <input id="email" type="email"
                                               class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                               placeholder="Email"
                                               name="email" value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group ">
                                    {{--<label for="password"--}}
                                    {{--class=" col-form-label text-md-right">{{ __('Password') }}</label>--}}

                                    <div class="">
                                        <input id="password" type="password"
                                               class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               placeholder="Password"
                                               name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group ">
                                    {{--<label for="password-confirm"--}}
                                    {{--class=" col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

                                    <div class="">
                                        <input id="password-confirm" type="password" class="form-control"
                                               placeholder="Password Confirmation"
                                               name="password_confirmation" required>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Register') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Main Content -->

</div>
<!-- /HK Wrapper -->

<!-- jQuery -->
<script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('vendors/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<!-- Slimscroll JavaScript -->
<script src="{{ asset('dist/js/jquery.slimscroll.js') }}"></script>

<!-- Fancy Dropdown JS -->
<script src="{{ asset('dist/js/dropdown-bootstrap-extended.js') }}"></script>

<!-- FeatherIcons JavaScript -->
<script src="{{ asset('dist/js/feather.min.js') }}"></script>

<!-- Toggles JavaScript -->
<script src="{{ asset('vendors/jquery-toggles/toggles.min.js') }}"></script>
<script src="{{ asset('dist/js/toggle-data.js') }}"></script>

<!-- Init JavaScript -->
<script src="{{ asset('dist/js/init.js') }}"></script>
</body>


<!-- Mirrored from hencework.com/theme/mintos/signup-simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 14:11:16 GMT -->
</html>
