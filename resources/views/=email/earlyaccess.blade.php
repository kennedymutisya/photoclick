@component('mail::message')
# Hi!

You’re one of the first people to get access to Pichaclick, a service dedicated to help photographers
focus on what matters most. We provide a photographer portfolio, covers, galleries, client proofing and print sales
coupled with top notch customer service so that you can focus on what matters most. Photography!. You can use this special link to access the
registration page and start your pichaclick journey right away!
@component('mail::button', ['url' => 'https://pichaclick.co.ke/register','color' => 'success'])
    Registration Link
@endcomponent
Also, We’re eager to hear what you think. Please reply to this email at any time with feedback or any questions you
have for us. We’re all ears. 😃

Best,

Kennedy Mutisya <br>
for
PichaClick PLC
@endcomponent
