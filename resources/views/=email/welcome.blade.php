@component('mail::message')
Thank you for signing up,

Welcome to PichaClick. We're super-exited to have you around! We built PichaClick to simplify your online sales, proofing and delivery.
We tried a lot of tools over the years and none of them solved our management problems. PichaClick
is different.

Here is a quick reference to what you can do with your membership
+ Create a [collection](https://pichaclick.co.ke/collection)
+ Upload your [photos](https://pichaclick.co.ke/collection)
+ Configure your settings [settings](https://pichaclick.co.ke/collection)

Please reach out  if there's any way we can help you get started or even just say hello.

Happy exploring,<br>
Kennedy Mutisya <br>
Program Director, {{ config('app.name') }}
@endcomponent
