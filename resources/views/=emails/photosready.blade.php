@component('mail::message')
# Hi,

Your photos are ready. Use the button to download

{{$message}}
@component('mail::button', ['url' => 'https://pichaclick.co.ke'])
Download
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
