@component('mail::message')
# Hi Ken,

@component('mail::table')
    |        |          |
    | ------------- |:-------------:|
    | Name      | {{$name}}.      |
    | Email      | {{$email}} |
    | Phone      | {{$phone}} |
    | Message      | {{$message}} |
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
