@extends('layouts.backend')

@section('content')
    <!-- Container -->
    <div class="container mt-xl-50 mt-sm-30 mt-15">

        <div class="row">
            <div class="col-12 mb-25">
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#sendtoclient">
                    Send to client
                </button>

                <!-- Modal -->
                <div class="modal fade" id="sendtoclient" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Send</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <sejdcollection :collection="{{$collection}}"></sejdcollection>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="col-12">
                        <h4>{{ $collection->name }}</h4>

                    </div>
                    <div class="col-12">
                        <p>
                            {{$collection->date->toFormattedDateString()}}
                        </p>
                    </div>

                    <div class="col-12">
                        @if(isset($collection->photos->first()->path))
                            <img
                                src=" {{ 'https://'.env('AWS_BUCKET').'.'.'s3.'.env('AWS_DEFAULT_REGION').'.'.'amazonaws.com/'.'watermarked/' .$collection->photos->first()->path }}"
                                class="card-img-top">
                        @else
                            <div class="">
                                <svg class="card-img-top" width="100%"
                                     xmlns="http://www.w3.org/2000/svg" focusable="false"
                                     role="img" aria-label="Placeholder: Collection"><title>Collection</title>
                                    <rect width="100%" height="100%" fill="#868e96"></rect>
                                </svg>

                            </div>
                        @endif
                    </div>
                    <div class="col-12">
                        <ul class="nav">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#photo">
                                    <i class="fa fa-picture-o"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " data-toggle="tab" href="#settings">
                                    <i class="fa fa-cog"></i>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " data-toggle="tab" href="#share">
                                    <i class="fa fa-rss fa-lg"></i>
                                </a>
                            </li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane container active" id="photo">
                                <div class="col-12">
                                    <h6>Photo</h6>
                                </div>
                            </div>
                            <div class="tab-pane container fade" id="settings">
                                <ul class="nav flex-column">
                                    <li class="nav-header">Settings</li>
                                    <li class="nav-link ">
                                        <a href=""><i class="fa fa-cog fa-fw fa-sm"></i>Collection Settings</a></li>
                                    <li class="nav-link ">
                                        <a href=""><i class="fa fa-bookmark fa-fw fa-sm"></i>Design</a></li>
                                    <li id="secondary-nav-privacy" class="nav-link ">
                                        <a href=""><i class="fa fa-lock fa-fw fa-sm"></i>Privacy</a> <span
                                            class="collection-secondary-nav-onoff label onoff label-default">off</span>
                                    </li>
                                    <li class="nav-link ">
                                        <a href=""><i class="fa fa-download fa-fw fa-sm"></i>Download</a> <span
                                            id="photo-download-setting-onoff"
                                            class="collection-secondary-nav-onoff label onoff label-default">on</span>
                                    </li>
                                    <li class="nav-link ">
                                        <a href=""><i class="fa fa-heart fa-fw fa-sm"></i>Favorite</a>
                                        <span id="favorite-setting-onoff"
                                              class="collection-secondary-nav-onoff label onoff label-default">on</span>
                                    </li>
                                    <li class="nav-link active">
                                        <a href=""><i class="fa fa-share fa-fw fa-sm"></i>Sharing</a> <span
                                            id="sharing-setting-onoff"
                                            class="collection-secondary-nav-onoff label onoff label-default">on</span>
                                    </li>
                                    <li class="nav-link ">
                                        <a href=""><i
                                                class="fa fa-shopping-cart fa-fw fa-sm"></i>Store</a> <span
                                            id="store-setting-onoff"
                                            class="collection-secondary-nav-onoff label onoff label-default">off</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane container fade" id="share">
                                <div class="col-xs-12">
                                    <ul class="nav nav-list flex-column vertical small">
                                        <li class="nav-header">Activities</li>
                                        <li class="nav-link ">
                                            <a href=""><i
                                                    class="fa fa-download fa-fw fa-sm"></i>Download Activity</a>
                                        </li>
                                        <li class="nav-link ">
                                            <a href=""><i
                                                    class="fa fa-heart fa-fw fa-sm"></i>Favorite Activity</a></li>
                                        <li class="nav-link ">
                                            <a href=""><i
                                                    class="fa fa-eye-slash fa-fw fa-sm"></i>Private Photo
                                                Activity</a>
                                        </li>
                                        <li class="nav-link ">
                                            <a href=""><i
                                                    class="fa fa-envelope fa-fw fa-sm"></i>Email Registrations</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <showcollection :collection="{{$collection->id}}" :photos="{{ $collection->photos }}"></showcollection>
            </div>
        </div>

    </div>
@endsection

