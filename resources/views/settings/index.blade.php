@extends('layouts.backend')

@section('content')
    <!-- Container -->
    <div class="container mt-xl-50 mt-sm-30 mt-15">


        <div class="row">
         <div class="col-md-2">
             <ul class="nav flex-column">
                 <li class="nav-item">
                     <a class="nav-link active" href="#"><i class="fa fa-globe"></i> Domain</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link" href="#"><i class="fa fa-bullhorn"></i> Logos & Branding</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link" href="#"><i class="fa fa-tint"></i> Watermark</a>
                 </li>
{{--                 <li class="nav-item">--}}
{{--                     <a class="nav-link" href="#">Homepage</a>--}}
{{--                 </li>--}}
{{--                 <li class="nav-item">--}}
{{--                     <a class="nav-link" href="#">Defaults</a>--}}
{{--                 </li>--}}
{{--                 <li class="nav-item">--}}
{{--                     <a class="nav-link" href="#">Analytics</a>--}}
{{--                 </li>--}}
{{--                 <li class="nav-item">--}}
{{--                     <a class="nav-link" href="#">Preferences</a>--}}
{{--                 </li>--}}
             </ul>
         </div>
         <div class="col-md-8">
             <settingsindex></settingsindex>
         </div>
        </div>

    </div>
@endsection
