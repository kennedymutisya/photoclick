<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>PichaClick - Gallery</title>
    <meta content="" name="description">
    <meta content="" name="author">
    <meta content="" name="keywords">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1" name="viewport">
    <!-- icon -->
    <link href="asset/images/favicon.html" rel="icon" sizes="32x32" type="image/png">
    <!-- plugin css -->
    <link href="{{ asset('gallery/plugin/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('gallery/plugin/css/slidercss.css') }}" rel="stylesheet">
    <link href="{{ asset('gallery/plugin/css/lightgallery.css') }}" rel="stylesheet">
    <!-- font themify CSS -->
    <link href="{{ asset('gallery/assets/css/themify-icons.css') }}" rel="stylesheet">
    <!-- font awesome CSS -->
    <link href="{{ asset('gallery/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <!-- main css -->
    <link href="{{ asset('gallery/assets/css/scroll.css') }}" rel="stylesheet">
    <link href="{{ asset('gallery/assets/css/animated-on3step.css') }}" rel="stylesheet">
    <link href="{{ asset('gallery/plugin/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ asset('gallery/plugin/css/owl.theme.css') }}" rel="stylesheet">
    <link href="{{ asset('gallery/assets/css/on3step-style.css') }}" rel="stylesheet">
    <link href="{{ asset('gallery/assets/css/queries-on3step.css') }}" media="all" rel="stylesheet">
    <link href="{{ asset('flex-images.css') }}" media="all" rel="stylesheet">
</head>
<body>

<!-- preloader -->
<div class="backgroundloader">
    <div class="loader text-center">
        <div id="infinity" class="onStep" data-animation="fadeInUp" data-time="0"></div>
        <div class="caption onStep" data-animation="fadeInUp" data-time="300">loading</div>
    </div>
</div>
<!-- preloader end -->

<!-- container -->
<div class="content-wrapper">
    <!-- header -->
    <!-- header -->
    <header class="init">
        <!-- nav -->
        <div class="navbar-default-white navbar-fixed-top">
            <!-- container -->
            <div class="container-fluid">
                <div class="row p-4-vh">
                    <!-- logo -->
                    <a class="navbar-brand white" href="#">
                        <img alt="logo" src="{{ asset('logo1.png') }}">
                    </a>
                    <!-- logo end -->
                </div>
            </div>
            <!-- container end -->
        </div>
        <!-- nav end -->
    </header>
    <section aria-label="works" id="Gallery" class="no-padding onStep" data-animation="fadeInUp" data-time="600">
        <div class="container-fluid">
            <div class="row">
                <div class="flex-images">
                    @foreach($photos as $photo)
                        <div class="item" data-w="279" data-h="180">
                            <img src="{{ 'https://mutisya.s3.us-east-2.amazonaws.com/'. $photo->path }}">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
    <!-- section gallery end -->


    <!-- footer -->
    <footer>
        <div class="container-fluid">
            <div class="row p-4-vh">
                <div class="left">
                    <div class="widget footer-links">
                        <ul class="list-menu">
                            <li><a class="active icon" href="#">ENG</a></li>
                            <li><a class="icon" href="#">FRA</a></li>
                            <li><a class="icon mr-0" href="#">GER</a></li>
                        </ul>
                    </div>
                    <h6>OUR INSTAGRAM</h6>
                    <div id="instafeed"></div>
                    <a href="#">@ors_cope</a>
                </div>
                <div class="right">
                    <div class="search-container">
                        <form action="#">
                            <input class="s-input-home" type="text" placeholder="Subscribe..." name="search">
                            <button class="btn-s-input" type="submit"><i class="fa fa-paper-plane"></i></button>
                        </form>
                    </div>
                    <div class="addres">
                        <p>Imenti House, Nairobi</p>
                        <a class="m-0" href="#">asagarama@gmail.com</a>
                    </div>
                </div>
            </div>
            <div class="row p-4-vh">
                <div class="space"></div>
            </div>
            <div class="row p-4-vh">
                <div class="right">
                    {{--<a class="icon" href="#">tw</a>--}}
                    {{--<a class="icon" href="#">fb</a>--}}
                    {{--<a class="icon" href="#">be</a>--}}
                    {{--<a class="icon" href="#">gh</a>--}}
                </div>
                <div class="left">
                    <span class="right">{{ date('Y') }} © PichaClick,  All Right Reserved</span>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->

    <!-- ScrolltoTop -->
    <div id="totop" class="init">
        &uarr;
    </div>

</div>
<!-- container end -->

<!-- plugin js -->
<script src="{{ asset('gallery/plugin/js/pluginson3step.js') }}"></script>
<script src="{{ asset('gallery/plugin/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('gallery/plugin/js/sticky.js') }}"></script>
<script src="{{ asset('gallery/plugin/js/slider.js') }}"></script>
<script src="{{ asset('gallery/plugin/js/lightgallery.js') }}"></script>
<!-- slider revolution  -->
<script src="{{ asset('gallery/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('gallery/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
<!-- main js -->
<script src="{{ asset('gallery/assets/js/on3step.js') }}"></script>
<script src="{{ asset('gallery/assets/js/plugin-set.js') }}"></script>
<!-- instafeed js -->
<script src='{{ asset('gallery/plugin/js/instafeed.min.js') }}'></script>
<script src="{{ asset('gallery/assets/js/instgfeed.js') }}"></script>
<script src="{{ asset('flex-images.js') }}"></script>

<script>
    new flexImages({selector: '.flex-images', rowHeight: 360});
</script>
</body>

</html>
