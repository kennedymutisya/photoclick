/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// require('vue2-animate');
// window.lightGallery = require('lightgallery.js');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));
//
// Vue.component('createcollection', require('./components/collection/create').default);
// Vue.component('asw', require('./components/collection/index.vue'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
window.Bus = new Vue();
const app = new Vue({
    el: '#app',
    created() {
    }
});

const front = new Vue({
    el: '#front',
    data: {
        email: '',
        contactname: '',
        contactemail: '',
        contactphone: '',
        contactmessage: '',
        successful: false,
        submitting: false
    },
    methods: {
        submitNewsletter() {
            // Send an email to the user for signup
            this.submitting = true;
            axios.post('/earlyaccess', this.$data).then(resp => {
                this.email = '';
                this.submitting = false;
                this.successful = true;
            }).catch(err => {
                this.submitting = false;
            });
        },
        submitticket() {
            axios.post('/mail', this.$data).then(resp => {
                this.contactname = '';
                this.contactemail = '';
                this.contactphone = '';
                this.contactmessage = '';
            }).catch(err => {

            });
        }
    }
});
