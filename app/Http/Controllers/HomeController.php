<?php

namespace App\Http\Controllers;

use App\Collection;
use Aws\Rekognition\RekognitionClient;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageManager;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('gallery');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function pic()
    {
        // WatermarkedPhoto
        $image = new ImageManager();
        $watermark = $image->make(public_path('logoblack.png'));
        $img = $image->make(public_path('app/collectionPhotos/ken.jpg'));
//#1
        $watermarkSize = $img->width() - 20; //size of the image minus 20 margins
//#2
        $watermarkSize = $img->width() / 2; //half of the image size
//#3
        $resizePercentage = 70;//70% less then an actual image (play with this value)
        $watermarkSize = round($img->width() * ((100 - $resizePercentage) / 100),
            2); //watermark will be $resizePercentage less then the actual width of the image

// resize watermark width keep height auto
        $watermark->resize($watermarkSize, null, function ($constraint) {
            $constraint->aspectRatio();
        });
//insert resized watermark to image center aligned
        $img->insert($watermark, 'center');
//save new image
//        $img->save(storage_path('app/images/watermark-test.jpg'));
        return $img->response('png');
    }

    public function gallery($gallery)
    {
        $photos = \App\Collection::withoutGlobalScope('user')->find($gallery)->load('photos')['photos'];
        return view('gallery.show',compact('photos'));
    }
}
