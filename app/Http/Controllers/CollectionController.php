<?php

namespace App\Http\Controllers;

use App\Collection;
use App\Collectionphoto;
use Illuminate\Http\Request;

class CollectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Collection $collection)
    {
        $collections = request()->user()->collection->load('photos');
        return view('collection.index',compact('collections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(Request $request,Collection $collection)
    {
        $this->validate($request,[
            'name'=> ['required','unique:collections,name'],
            'eventdate' => ['date']
        ],[
            'name.required' => 'Provide a Collection Name',
            'name.unique' => 'A Collection with the same name already exists',
            'date.date' => 'Please provide a valid date',
        ]);

        $collection->name = $request->name;
        $collection->user_id = $request->user()->id;
        $collection->date = $request->eventdate;
        $collection->saveOrFail();

//       $allofThem= Collection::find($collection->id)->with('photos')->withCount('photos')->get();

        return response()->json($collection->load('photos'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function show(Collection $collection)
    {
        $collection->load('photos');
        return view('collection.show',compact('collection'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function edit(Collection $collection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collection $collection)
    {
        $collection->name = $request->name;
        $collection->user_id = $request->user()->id;
        $collection->date = $request->eventdate;
        $collection->update();

        return response()->json($collection);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Collection  $collection
     * @return \Illuminate\Http\Response
     */
    public function destroy(Collection $collection)
    {
        $collection->delete();
        return response()->json($collection);
    }
}
