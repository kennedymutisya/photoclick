<?php


namespace App\Events;


use App\Mail\WelcomeMail;

class UserEvents
{
    public function sendWelcomeEmail($event)
    {
        \Mail::to(collect($event)->first()->email)->send(new WelcomeMail());
    }
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Registered',
            'App\Events\UserEvents@sendWelcomeEmail'
        );

    }
}
